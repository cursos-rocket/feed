import { Header } from "./components/Header"
import { Sidebar } from "./components/Sidebar";

import styles from "./App.module.css";
import "./global.css";
import { Post, PostType } from "./components/Post";


const posts: PostType[] = [
  {
    id: 1,
    author: {
      avatarUrl: "https://github.com/lucasaol.png",
      name: "Lucas Andrade",
      role: "Software Engineer @devlucas"
    },
    content: [
      {type: "paragraph", content: "Fala galera"},
      {type: "paragraph", content: "Acabei de subir mais um projeto no meu portifólio. É um projeto bla bla bla..."},
      {type: "link", content: "jane.design/doctorcare"}
    ],
    publishedAt: new Date('2024-03-11 19:37:00'),
    comments: [
      {
        id: 1,
        content: "muito bom parabens"
      },
      {
        id: 2,
        content: "uaau"
      }
    ]
  },
  {
    id: 2,
    author: {
      avatarUrl: "https://github.com/diego3g.png",
      name: "Diego Fernandes",
      role: "CTO @Rocketseat"
    },
    content: [
      {type: "paragraph", content: "Fala galera"},
      {type: "paragraph", content: "Acabei de subir mais um projeto no meu portifólio. É um projeto bla bla bla..."},
      {type: "link", content: "jane.design/doctorcare"}
    ],
    publishedAt: new Date('2024-03-10 12:00:00'),
    comments: [
      {
        id: 1,
        content: "muito bom parabens"
      }
    ]
  },
  {
    id: 3,
    author: {
      avatarUrl: "https://github.com/maykbrito.png",
      name: "Mayk Brito",
      role: "Educator @Rocketseat"
    },
    content: [
      {type: "paragraph", content: "Fala galera"},
      {type: "paragraph", content: "Acabei de subir mais um projeto no meu portifólio. É um projeto bla bla bla..."},
      {type: "link", content: "jane.design/doctorcare"}
    ],
    publishedAt: new Date('2024-03-10 15:30:00'),
    comments: [
    ]
  }
];

export function App() {

  return (
    <>
    <Header />

    <div className={styles.wrapper}>
      <Sidebar />
      <main>
        {posts.map(post => {
          return (
            <Post 
              key={post.id}
              post={post}
            />
          )
        })}

      </main>
    </div>
    </>
  )
}

