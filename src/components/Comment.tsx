import { ThumbsUp, Trash } from "phosphor-react";
import styles from "./Comment.module.css";
import { Avatar } from "./Avatar";
import { useState } from "react";

interface CommentProps {
    id: number;
    content: string;
    onDeleteComment: (commentId: number) => void;
}

export function Comment({ id, content, onDeleteComment }: CommentProps) {
    const [likes, setLikes] = useState(0);

    return (
        <div className={styles.comment}>
        <Avatar src="https://github.com/lucasaol.png" hasBorder={false} alt="" />

            <div className={styles.commentBox}>
                <div className={styles.commentContent}>
                    <header>
                        <div className={styles.authorAndTime}>
                            <strong>Lusca Andrade</strong>
                            <time title="06 de março às 20:36" dateTime="2024-03-06 20:36:19">Cerca de 1h atrás</time>
                        </div>

                        <button onClick={() => onDeleteComment(id)} title="Deletar comentário">
                            <Trash size={24} />
                        </button>
                    </header>
                    <p>{content}</p>
                </div>
                <footer>
                    
                    <button onClick={() => setLikes((state) => {return state + 1; })}>
                        <ThumbsUp size={20} />
                        Aplaudir <span>{likes}</span>
                    </button>
                </footer>
            </div>
        </div>
    );
}